import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProcessamentoComponent } from './processamento/processamento.component';

const routes: Routes = [
  { path: '', component: ProcessamentoComponent },
  { path: '**', redirectTo: 'pagina-nao-encontrada' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
