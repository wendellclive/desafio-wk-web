import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';
import { NavBarComponent } from './core/navbar/navbar.component';
import { ProcessamentoComponent } from './processamento/processamento.component';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { StepsModule } from 'primeng/steps';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ProcessaService } from './processa.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ProcessamentoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule,
    CardModule,
    TableModule,
    StepsModule,
    ProgressSpinnerModule,
    HttpClientModule
  ],
  providers: [HttpClient, ProcessaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
