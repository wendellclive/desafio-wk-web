import { CandidatosEstado, ImcPorFaixaEtaria, ObesosEntreHomensMulheres, TipoSanguineo } from './../model/model'
import { ProcessaService } from '../processa.service'
import { Component } from '@angular/core'
import { MenuItem } from 'primeng/api/menuitem'
import { Subscription } from 'rxjs/internal/Subscription'
import { interval } from 'rxjs/internal/observable/interval'


@Component({
  selector: 'app-processamento',
  templateUrl: './processamento.component.html',
  styleUrls: ['./processamento.component.scss']
})
export class ProcessamentoComponent {

  started: boolean = false
  steps: MenuItem[] = [
    { label: 'Conectando' },
    { label: 'Importando Dados' },
    { label: 'Gravando no Banco de Dados' },
    { label: 'Concluído' },
  ]

  activeIndex = 0
  ativaSteps: boolean = false
  processando: boolean = false
  candidadosEstado: CandidatosEstado[] = []
  imcMedioPorFaixaEtaria: ImcPorFaixaEtaria[] = []
  obesosEntreHomensMulheres: ObesosEntreHomensMulheres[] = []
  mediaIdadeTipoSanguineo: TipoSanguineo[] = []
  possiveisDoadoresPorReceptor:TipoSanguineo[] = []

  private subscription!: Subscription

  constructor(
    private processo: ProcessaService
  ) { }

  capturarDadosWeb() {

    this.ativaSteps = true
    this.processando = true

    this.processo.capturarDadosWeb()

    this.subscription = interval(1000).subscribe(() => {
      this.activeIndex = (this.activeIndex + 1) % this.steps.length

      if (this.activeIndex === 3) {
        this.subscription.unsubscribe()
        this.processando = false
        this.started = true
      }
    })

    this.candidatosPorEstado()
    this.imcFaixaEtaria()
    this.obesosHomemMulher()
    this.mediaTipoSanguineo()
    this.possivelDoadorPorReceptor()
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }

  candidatosPorEstado() {

    this.processo.candidatosPorEstado().subscribe(
      result => {
        this.candidadosEstado = result
      }
    )
  }

  imcFaixaEtaria() {

    this.processo.imcFaixaEtaria().subscribe(
      result => {
        this.imcMedioPorFaixaEtaria = result
      }
    )
  }

  obesosHomemMulher() {

    this.processo.obesosHomemMulher().subscribe(
      result => {
        this.obesosEntreHomensMulheres = result
      }
    )
  }

  mediaTipoSanguineo() {

    this.processo.tipoSanguineo("tipoSanguineoMedia").subscribe(
      result => {
        this.mediaIdadeTipoSanguineo = result
      }
    )
  }

  possivelDoadorPorReceptor() {

    this.processo.tipoSanguineo("possiveisDoadoresReceptor").subscribe(
      result => {
        this.possiveisDoadoresPorReceptor = result
      }
    )
  }

}

