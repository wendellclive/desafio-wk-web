export interface FaixaEtaria {

  faixa0a10: string;
  faixa11a20: string;
  faixa21a30: string;
  faixa31a40: string;
  faixa41a50: string;
  faixa51a50: string;
  faixa61a50: string;
  faixa71a50: string;
  faixa81a50: string;
  faixa91a50: string;
  faixa101a110: string;

}

export interface CandidatosEstado {

  estado: string;
  totalUsuarios: string;

}

export interface ImcPorFaixaEtaria {
  faixaIdade: string,
  imcMedio: string
}

export interface ObesosEntreHomensMulheres {
  sexo: string
  total: string
  obesos: string
  percentual: string
}

export interface TipoSanguineo {
  tipoSanguineo: string;
  valor: string;
}
