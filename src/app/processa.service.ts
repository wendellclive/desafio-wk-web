import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';
import { environment } from 'src/environment/environment';
import { CandidatosEstado, ImcPorFaixaEtaria, ObesosEntreHomensMulheres, TipoSanguineo } from './model/model';

@Injectable({
  providedIn: 'root'
})
export class ProcessaService {

  apiUrl = `${environment.apiUrl}/api/doador`

  constructor(
    private http: HttpClient
  ) { }

  async capturarDadosWeb(): Promise<void> {
    const response = await this.http.post<any>(this.apiUrl + "/importar-dados", null)
      .toPromise();
    return response;
  }

  candidatosPorEstado(): Observable<CandidatosEstado[]> {
    return this.http.get<CandidatosEstado[]>(this.apiUrl + "/candidatos").pipe(
      map(response => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error)
      }));
  }

  imcFaixaEtaria(): Observable<ImcPorFaixaEtaria[]> {
    return this.http.get<ImcPorFaixaEtaria[]>(this.apiUrl + "/imcPorFaixaEtaria").pipe(
      map(response => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error)
      }));
  }

  obesosHomemMulher(): Observable<ObesosEntreHomensMulheres[]> {
    return this.http.get<ObesosEntreHomensMulheres[]>(this.apiUrl + "/obesoHomemMulher").pipe(
      map(response => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error)
      }));
  }

  tipoSanguineo(chamada: string) {
    return this.http.get<TipoSanguineo[]>(this.apiUrl + "/" + chamada).pipe(
      map(response => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error)
      }));
  }

}
